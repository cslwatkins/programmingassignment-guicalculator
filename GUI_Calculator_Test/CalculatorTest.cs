using GUI_Calculator_Classes;
namespace GUI_Calculator_Test
{
    [TestClass]
    public class CalculatorTest
    {
        public const string powersResult = "25";
        public const string powersResult2 = "55.9016994";
        public const string sqrtResult = "5";
        public const string sqrtResult2 = "2.236068";
        public const string divideResult = "2.5";
        public const string multiplyResult = "5";
        public const string addResult = "25";
        public const string subtractResult = "5.4";
        public const string operatorMapResult = "3,+.6,-.7,SQRT().15,*.17,/.19,^.";
        public const int DictSearchResult = 5;
        public const int DictSearchResult2 = 3;
        public const int DictSearchResult3 = 6;

        [TestMethod]
        public void OperatorMapperTest()
        {
            bool sqrt;
            bool power;
            bool divide;
            bool multiply;
            bool add;
            bool subtract;
            string testInput = "523+62-SQRT(53)*2/5^2";
            Dictionary<int, string> testResult = new Dictionary<int, string>();
            Calculator.OperatorMapper(testInput, out testResult, out sqrt, out power, out divide, out multiply, out add, out subtract);
            string result = "";
            foreach (int i in testResult.Keys)
            {
                result = result + i.ToString() + "," + testResult[i] + ".";
            }

            Assert.AreEqual(operatorMapResult, result);
        }

        [TestMethod]
        public void PowersCalcTest()
        {
            string result = "";
            Calculator.PowersHandler("5^2", out result);
            Assert.AreEqual(powersResult, result);
        }

        [TestMethod]
        public void PowersCalcTest2()
        {
            string result = "";
            Calculator.PowersHandler("5^2.5", out result);
            Assert.AreEqual(powersResult2, result);
        }

        [TestMethod]
        public void SqrtCalcTest()
        {
            string result = "";
            Calculator.SqrtHandler("SQRT(25", out result);
            Assert.AreEqual(sqrtResult, result);
        }

        [TestMethod]
        public void SqrtCalcTest2()
        {
            string result = "";
            Calculator.SqrtHandler("SQRT(5", out result);
            Assert.AreEqual(sqrtResult2, result);
        }

        [TestMethod]
        public void DivideCalcTest()
        {
            string result = "";
            Calculator.DivisionHandler("5/2", out result);
            Assert.AreEqual(divideResult, result);
        }

        [TestMethod]
        public void MultiplyCalcTest()
        {
            string result = "";
            Calculator.MultiplicationHandler("2.5*2", out result);
            Assert.AreEqual(multiplyResult, result);
        }

        [TestMethod]
        public void AddCalcTest()
        {
            string result = "";
            Calculator.AddHandler("5.4+19.6", out result);
            Assert.AreEqual(addResult, result);
        }

        [TestMethod]
        public void SubtractCalcTest()
        {
            string result = "";
            Calculator.SubtractHandler("25-19.6", out result);
            Assert.AreEqual(subtractResult, result);
        }

        [TestMethod]
        public void DictionarySearchTest()
        {
            int result;
            int resultBefore;
            int resultAfter;
            Dictionary<int, string> testInput = new Dictionary<int, string>();
            testInput.Add(3, "+");
            testInput.Add(5, "/");
            testInput.Add(6, "-");
            testInput.Add(9, "SQRT()");
            Calculator.DictionarySearch(testInput, "/", out result, out resultBefore, out resultAfter);

            Assert.AreEqual(DictSearchResult, result);
        }

        [TestMethod]
        public void DictionarySearchTest2()
        {
            int result;
            int resultBefore;
            int resultAfter;
            Dictionary<int, string> testInput = new Dictionary<int, string>();
            testInput.Add(3, "+");
            testInput.Add(5, "/");
            testInput.Add(6, "-");
            testInput.Add(9, "SQRT()");
            Calculator.DictionarySearch(testInput, "/", out result, out resultBefore, out resultAfter);

            Assert.AreEqual(DictSearchResult2, resultBefore);
        }

        [TestMethod]
        public void DictionarySearchTest3()
        {
            int result;
            int resultBefore;
            int resultAfter;
            Dictionary<int, string> testInput = new Dictionary<int, string>();
            testInput.Add(3, "+");
            testInput.Add(5, "/");
            testInput.Add(6, "-");
            testInput.Add(9, "SQRT()");
            Calculator.DictionarySearch(testInput, "/", out result, out resultBefore, out resultAfter);

            Assert.AreEqual(DictSearchResult3, resultAfter);
        }

    }
}