﻿using GUI_Calculator_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Calculator_Test
{
    [TestClass]
    public class StringHandlingTest
    {
        public const string extractResult = "Result";
        public const string splitResult1 = "First";
        public const string splitResult2 = "Second";
        public const string shrinkResult = "BeginningReplacementEnding";

        [TestMethod]
        public void StringSplitterTest()
        {
            string testInput = "First^Second";
            char op = '^';
            string subString1;
            string subString2;
            StringHandling.StringSplitter(testInput, '^', out subString1, out subString2);

            Assert.AreEqual(splitResult1, subString1, "String 1 failed");
            Assert.AreEqual(splitResult2, subString2, "String 2 failed");
            // There are 2 asserts as 1 action generates 2 results, this can be separated with messages so you can tell which assert threw the exception.
        }

        [TestMethod]
        public void StringExtractingTest()
        {
            string testInput = "TheResultIs";
            int startIndex = 3;
            int endIndex = 8;
            string output = "";
            StringHandling.StringExtractor(testInput, startIndex, endIndex, out output);

            Assert.AreEqual(extractResult, output);
        }

        [TestMethod]
        public void StringShrinkingTest()
        {
            string testInput = "BeginningMiddleEnding";
            string toinput = "Replacement";
            int start = 9;
            int end = 14;
            string result = StringHandling.StringShrink(testInput, toinput, start, end);

            Assert.AreEqual(shrinkResult, result);
        }
    }
}
