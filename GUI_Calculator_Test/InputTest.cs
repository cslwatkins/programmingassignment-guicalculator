﻿using GUI_Calculator_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Calculator_Test
{
    [TestClass]
    public class InputTest
    {
        [TestMethod]
        public void GetIntTest()
        {
            string testInput = "F";
            Assert.ThrowsException<Exception> (() => Input.GetInt(testInput));

        }

        [TestMethod]
        public void GetIntTest2()
        {
            string testInput = "Test";
            Assert.ThrowsException<Exception>(() => Input.GetInt(testInput));

        }

        [TestMethod]
        public void GetIntTest3()
        {
            string testInput = "22";
            bool result;
            result = Input.GetInt(testInput);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GetIntTest4()
        {
            string testInput = "7";
            bool result;
            result = Input.GetInt(testInput);

            Assert.IsTrue(result);
        }

    }
}
