﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Calculator_Classes
{
    public static class StringHandling
    {
        public static void StringExtractor(string input, int start, int end, out string section)
        {
            // Extract a part of a string by specifying the start and end index
            section = input.Substring(start, (end - start + 1));
        }

        public static void StringSplitter(string input, char separator, out string subString1, out string subString2)
        {
            // Take a string and split it either side of a set character (the first instance of this character)
            int index = input.IndexOf(separator); // Get the index of the character between the 2 strings
            subString1 = input.Substring(0, index); // Extract the first string
            subString2 = input.Remove(0, index + 1); // Extract the second string
        }

        public static string StringShrink(string input, string replacement, int startIndex, int endIndex)
        {
            // Take a string, remove the middle and replace that with another string
            string before = input.Substring(0, startIndex);
            string after = input.Substring(endIndex + 1);
            return before + replacement + after;
        }
    }
}
