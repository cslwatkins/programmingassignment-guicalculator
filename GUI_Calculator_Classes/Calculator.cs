﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Calculator_Classes
{
    public static class Calculator
    {
        public static void Calculate(string input, out string result)
        {
            // Method that holds all the other parts
            string tempString = new string(input); // Specify "new string" to ensure that is a separate object and not just passing the reference
            result = "";
            bool complete = false; // Controls the loop
            // Used to contain what operators exist in the string
            bool sqrt = true;
            bool power = true;
            bool divide = true;
            bool multipy = true;
            bool add = true;
            bool subtract = true;
            while (!complete)
            {
                // Create a list of where the operators are
                Dictionary<int, string> operPairs = new Dictionary<int, string>();
                OperatorMapper(tempString, out operPairs, out sqrt, out power, out divide, out multipy, out add, out subtract);
                if (operPairs.Count > 0)
                {
                    // If there is an operator to process
                    // Process in the BODMAS/BIDMAS order
                    if (sqrt)
                    {
                        int index;
                        int before;
                        int after;
                        string toProcess;
                        string tempResult;
                        // Find the location of the Operator and the next operator
                        DictionarySearch(operPairs, "SQRT()", out index, out before, out after);
                        if (after == 0) { after = tempString.Length - 2; }
                        else { after--; }
                        // Extract using the indexes
                        StringHandling.StringExtractor(tempString, index, after, out toProcess);
                        // Pass extracted part to the method to process the calculation
                        SqrtHandler(toProcess, out tempResult);
                        after++;
                        // Replace the part of the string that was calculated with the result
                        tempString = StringHandling.StringShrink(tempString, tempResult, index, after);
                    }
                    // The rest follow the same setup as sqrt, but are extracting from between the operator before the target and the operator after
                    else if (power)
                    {
                        int index;
                        int before;
                        int after;
                        string toProcess;
                        string tempResult;
                        DictionarySearch(operPairs, "^", out index, out before, out after);
                        if (after == 0) { after = tempString.Length - 1; }
                        else { after--; }
                        if (before != 0) { before++; }
                        StringHandling.StringExtractor(tempString, before, after, out toProcess);
                        PowersHandler(toProcess, out tempResult);
                        tempString = StringHandling.StringShrink(tempString, tempResult, before, after);
                    }
                    else if (divide)
                    {
                        int index;
                        int before;
                        int after;
                        string toProcess;
                        string tempResult;
                        DictionarySearch(operPairs, "/", out index, out before, out after);
                        if (after == 0) { after = tempString.Length - 1; }
                        else { after--; }
                        if (before != 0) { before++; }
                        StringHandling.StringExtractor(tempString, before, after, out toProcess);
                        DivisionHandler(toProcess, out tempResult);
                        tempString = StringHandling.StringShrink(tempString, tempResult, before, after);
                    }
                    else if (multipy)
                    {
                        int index;
                        int before;
                        int after;
                        string toProcess;
                        string tempResult;
                        DictionarySearch(operPairs, "*", out index, out before, out after);
                        if (after == 0) { after = tempString.Length - 1; }
                        else { after--; }
                        if (before != 0) { before++; }
                        StringHandling.StringExtractor(tempString, before, after, out toProcess);
                        MultiplicationHandler(toProcess, out tempResult);
                        tempString = StringHandling.StringShrink(tempString, tempResult, before, after);
                    }
                    else if (add)
                    {
                        int index;
                        int before;
                        int after;
                        string toProcess;
                        string tempResult;
                        DictionarySearch(operPairs, "+", out index, out before, out after);
                        if (after == 0) { after = tempString.Length - 1; }
                        else { after--; }
                        if (before != 0) { before++; }
                        StringHandling.StringExtractor(tempString, before, after, out toProcess);
                        AddHandler(toProcess, out tempResult);
                        tempString = StringHandling.StringShrink(tempString, tempResult, before, after);
                    }
                    else if (subtract)
                    {
                        int index;
                        int before;
                        int after;
                        string toProcess;
                        string tempResult;
                        DictionarySearch(operPairs, "-", out index, out before, out after);
                        if (after == 0) { after = tempString.Length - 1; }
                        else { after--; }
                        if (before != 0) { before++; }
                        StringHandling.StringExtractor(tempString, before, after, out toProcess);
                        SubtractHandler(toProcess, out tempResult);
                        tempString = StringHandling.StringShrink(tempString, tempResult, before, after);
                    }
                }
                else
                {
                    // Reaches this when there are no more operators, and exits the loop
                    complete = true;
                }
            }
            result = tempString;

        }

        public static void OperatorMapper(string input, out Dictionary<int,string>  opIndexes, out bool sqrt, out bool power, out bool divide, out bool multiply, out bool add, out bool subtract)
        {
            opIndexes = new Dictionary<int, string>();
            sqrt = false;
            power = false;
            divide = false;
            multiply = false;
            add = false;
            subtract = false;
            // Step through each character with the index being easily accessible
            for (int i = 0; i < input.Length; i++ )
            {
                try
                {
                    // If it is a number, don't process any code
                    Int32.Parse(input[i].ToString());
                }
                catch
                {
                    // For each item, set it's corresponding bool to true
                    switch(input[i])
                    {
                        case '^':
                            power = true;
                            break;
                        case '/':
                            divide = true;
                            break;
                        case '*':
                            multiply = true;
                            break;
                        case '+':
                            add = true;
                            break;
                        case '-':
                            subtract = true;
                            break;
                        case 'S':
                            // Add SQRT to the dictionary the code the rest uses would just add an S for the operator
                            opIndexes.Add(i, "SQRT()");
                            sqrt = true;
                            while (input[i] != ')')
                            {
                                // Skip forwards to closing bracket
                                i++;
                            }
                            // Dont process rest of loop, go to next iteration
                            continue;
                        default:
                            continue;
                    }
                    // Add the index and operator to the dictionary
                    opIndexes.Add(i, input[i].ToString());
                }
            }
        }

        public static void PowersHandler(string input, out string resultString)
        {
            string tempString1;
            string tempString2;
            // This splits the string into the part holding each number
            StringHandling.StringSplitter(input, '^', out tempString1, out tempString2);
            try
            {
                // Parse it to a double then perform the calculation
                double tempValue1 = Double.Parse(tempString1);
                double tempValue2 = Double.Parse(tempString2);
                double result = Math.Pow(tempValue1, tempValue2);
                // Convert to string with 7 decimal places
                // This is to strip the minor inaccuracies when doing floating calculations in binary
                resultString = result.ToString("0.#######");
            }
            catch
            {
                throw new Exception();
            }
        }

        public static void SqrtHandler(string input, out string resultString)
        {
            int index = input.IndexOf("SQRT(");
            if (index == 0)
            {
                // Remove the junk at the beginning of the string, leaving just the number
                input = input.Remove(0, 5);
                try
                {
                    double value = Double.Parse(input);
                    double result = Math.Sqrt(value);
                    resultString = result.ToString("0.#######");
                }
                catch
                {
                    throw new Exception();
                }
            }
            else
            {
                throw new Exception();
            }
        }

        // Same as powers
        public static void DivisionHandler(string input, out string resultString)
        {
            string tempString1;
            string tempString2;
            StringHandling.StringSplitter(input,'/', out  tempString1, out tempString2);
            try
            {
                double tempValue1 = Double.Parse(tempString1);
                double tempValue2 = Double.Parse(tempString2);
                double result = (tempValue1 /  tempValue2);
                resultString = result.ToString("0.#######");
            }
            catch
            {
                throw new Exception();
            }
        }

        // Same as powers
        public static void MultiplicationHandler(string input, out string resultString)
        {
            string tempString1;
            string tempString2;
            StringHandling.StringSplitter(input, '*', out tempString1, out tempString2);
            try
            {
                double tempValue1 = Double.Parse(tempString1);
                double tempValue2 = Double.Parse(tempString2);
                double result = (tempValue1 * tempValue2);
                resultString = result.ToString("0.#######");
            }
            catch
            {
                throw new Exception();
            }
        }

        // Same as powers
        public static void AddHandler(string input, out string resultString)
        {
            string tempString1;
            string tempString2;
            StringHandling.StringSplitter(input, '+', out tempString1, out tempString2);
            try
            {
                double tempValue1 = Double.Parse(tempString1);
                double tempValue2 = Double.Parse(tempString2);
                double result = (tempValue1 + tempValue2);
                resultString = result.ToString("0.#######");
            }
            catch
            {
                throw new Exception();
            }
        }

        // Same as powers
        public static void SubtractHandler(string input, out string resultString)
        {
            string tempString1;
            string tempString2;
            StringHandling.StringSplitter(input, '-', out tempString1, out tempString2);
            try
            {
                double tempValue1 = Double.Parse(tempString1);
                double tempValue2 = Double.Parse(tempString2);
                double result = (tempValue1 - tempValue2);
                resultString = result.ToString("0.#######");
            }
            catch
            {
                throw new Exception();
            }
        }

        public static void DictionarySearch(Dictionary<int, string> searchArea, string searchString, out int target, out int before, out int after)
        {
            // Find the first instance of the string (operator)
            target = 0;
            before = 0;
            after = 0;
            int index;
            bool complete = false;
            foreach (int i in searchArea.Keys)
            {
                if (searchArea[i] == searchString)
                {
                    target = i;
                    complete = true;
                    break;
                }
            }
            // Get the index of the operator before and after the desired one
            if (complete)
            {
                List<int> keys = searchArea.Keys.ToList();
                index = keys.IndexOf(target);
                if (index != 0)
                {
                    before = keys[index - 1];
                }
                if (keys.Count != index + 1)
                {
                    after = keys[index + 1];
                }
            }
        }
    }
}
