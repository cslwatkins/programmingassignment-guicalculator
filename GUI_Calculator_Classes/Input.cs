﻿namespace GUI_Calculator_Classes
{
    public static class Input
    {
        public static bool GetInt(string input)
        {
            // Take an input and validate that it is a number between 0 and 9
            try
            {
                int number = Int32.Parse(input);
                if (number >= 0 && number <= 9)
                {
                    return true;
                }
                else { return false; }
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}
