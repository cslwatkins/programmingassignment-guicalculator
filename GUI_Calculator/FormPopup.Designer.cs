﻿namespace GUI_Calculator
{
    partial class FormPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            InputString = new TextBox();
            RequestLabel = new Label();
            button1 = new Button();
            button2 = new Button();
            button3 = new Button();
            button4 = new Button();
            button5 = new Button();
            button6 = new Button();
            button7 = new Button();
            button8 = new Button();
            button9 = new Button();
            button0 = new Button();
            buttonDP = new Button();
            buttonSubmit = new Button();
            StatusLabel = new Label();
            Cancel = new Button();
            Backspace = new Button();
            SuspendLayout();
            // 
            // InputString
            // 
            InputString.Location = new Point(67, 103);
            InputString.Name = "InputString";
            InputString.Size = new Size(418, 39);
            InputString.TabIndex = 0;
            // 
            // RequestLabel
            // 
            RequestLabel.AutoSize = true;
            RequestLabel.Location = new Point(103, 39);
            RequestLabel.Name = "RequestLabel";
            RequestLabel.Size = new Size(353, 32);
            RequestLabel.TabIndex = 1;
            RequestLabel.Text = "Enter the number to squareroot";
            // 
            // button1
            // 
            button1.Location = new Point(168, 319);
            button1.Name = "button1";
            button1.Size = new Size(70, 70);
            button1.TabIndex = 2;
            button1.Text = "1";
            button1.UseVisualStyleBackColor = true;
            button1.Click += InputButton_Click;
            // 
            // button2
            // 
            button2.Location = new Point(244, 319);
            button2.Name = "button2";
            button2.Size = new Size(70, 70);
            button2.TabIndex = 3;
            button2.Text = "2";
            button2.UseVisualStyleBackColor = true;
            button2.Click += InputButton_Click;
            // 
            // button3
            // 
            button3.Location = new Point(320, 319);
            button3.Name = "button3";
            button3.Size = new Size(70, 70);
            button3.TabIndex = 4;
            button3.Text = "3";
            button3.UseVisualStyleBackColor = true;
            button3.Click += InputButton_Click;
            // 
            // button4
            // 
            button4.Location = new Point(168, 243);
            button4.Name = "button4";
            button4.Size = new Size(70, 70);
            button4.TabIndex = 5;
            button4.Text = "4";
            button4.UseVisualStyleBackColor = true;
            button4.Click += InputButton_Click;
            // 
            // button5
            // 
            button5.Location = new Point(244, 243);
            button5.Name = "button5";
            button5.Size = new Size(70, 70);
            button5.TabIndex = 6;
            button5.Text = "5";
            button5.UseVisualStyleBackColor = true;
            button5.Click += InputButton_Click;
            // 
            // button6
            // 
            button6.Location = new Point(320, 243);
            button6.Name = "button6";
            button6.Size = new Size(70, 70);
            button6.TabIndex = 7;
            button6.Text = "6";
            button6.UseVisualStyleBackColor = true;
            button6.Click += InputButton_Click;
            // 
            // button7
            // 
            button7.Location = new Point(168, 167);
            button7.Name = "button7";
            button7.Size = new Size(70, 70);
            button7.TabIndex = 8;
            button7.Text = "7";
            button7.UseVisualStyleBackColor = true;
            button7.Click += InputButton_Click;
            // 
            // button8
            // 
            button8.Location = new Point(244, 167);
            button8.Name = "button8";
            button8.Size = new Size(70, 70);
            button8.TabIndex = 9;
            button8.Text = "8";
            button8.UseVisualStyleBackColor = true;
            button8.Click += InputButton_Click;
            // 
            // button9
            // 
            button9.Location = new Point(320, 167);
            button9.Name = "button9";
            button9.Size = new Size(70, 70);
            button9.TabIndex = 10;
            button9.Text = "9";
            button9.UseVisualStyleBackColor = true;
            button9.Click += InputButton_Click;
            // 
            // button0
            // 
            button0.Location = new Point(168, 395);
            button0.Name = "button0";
            button0.Size = new Size(70, 70);
            button0.TabIndex = 11;
            button0.Text = "0";
            button0.UseVisualStyleBackColor = true;
            button0.Click += InputButton_Click;
            // 
            // buttonDP
            // 
            buttonDP.Location = new Point(244, 395);
            buttonDP.Name = "buttonDP";
            buttonDP.Size = new Size(70, 70);
            buttonDP.TabIndex = 12;
            buttonDP.Text = ".";
            buttonDP.UseVisualStyleBackColor = true;
            buttonDP.Click += InputButton_Click;
            // 
            // buttonSubmit
            // 
            buttonSubmit.Location = new Point(168, 471);
            buttonSubmit.Name = "buttonSubmit";
            buttonSubmit.Size = new Size(222, 70);
            buttonSubmit.TabIndex = 13;
            buttonSubmit.Text = "Submit";
            buttonSubmit.UseVisualStyleBackColor = true;
            buttonSubmit.Click += ButtonSubmit_Click;
            // 
            // StatusLabel
            // 
            StatusLabel.AutoSize = true;
            StatusLabel.ForeColor = Color.Green;
            StatusLabel.Location = new Point(168, 619);
            StatusLabel.Name = "StatusLabel";
            StatusLabel.Size = new Size(148, 32);
            StatusLabel.TabIndex = 14;
            StatusLabel.Text = "Status: Good";
            // 
            // Cancel
            // 
            Cancel.Location = new Point(168, 546);
            Cancel.Name = "Cancel";
            Cancel.Size = new Size(222, 70);
            Cancel.TabIndex = 15;
            Cancel.Text = "Cancel";
            Cancel.UseVisualStyleBackColor = true;
            Cancel.Click += Cancel_Click;
            // 
            // Backspace
            // 
            Backspace.Font = new Font("Segoe UI", 8F);
            Backspace.Location = new Point(320, 395);
            Backspace.Name = "Backspace";
            Backspace.Size = new Size(70, 70);
            Backspace.TabIndex = 16;
            Backspace.Text = "Backspace";
            Backspace.UseVisualStyleBackColor = true;
            Backspace.Click += Delete_Click;
            // 
            // FormPopup
            // 
            AutoScaleDimensions = new SizeF(13F, 32F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(574, 679);
            Controls.Add(Backspace);
            Controls.Add(Cancel);
            Controls.Add(StatusLabel);
            Controls.Add(buttonSubmit);
            Controls.Add(buttonDP);
            Controls.Add(button0);
            Controls.Add(button9);
            Controls.Add(button8);
            Controls.Add(button7);
            Controls.Add(button6);
            Controls.Add(button5);
            Controls.Add(button4);
            Controls.Add(button3);
            Controls.Add(button2);
            Controls.Add(button1);
            Controls.Add(RequestLabel);
            Controls.Add(InputString);
            Name = "FormPopup";
            Text = "Calculator";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox InputString;
        private Label RequestLabel;
        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        private Button button5;
        private Button button6;
        private Button button7;
        private Button button8;
        private Button button9;
        private Button button0;
        private Button buttonDP;
        private Button buttonSubmit;
        private Label StatusLabel;
        private Button Cancel;
        private Button Backspace;
    }
}