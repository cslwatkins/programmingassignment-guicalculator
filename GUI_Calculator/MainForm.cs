using GUI_Calculator.Tools;
using GUI_Calculator_Classes;
using System.Drawing.Text;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace GUI_Calculator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Calculate_Click(object sender, EventArgs e)
        {
            // Take the text in the current calculation box
            string calculation = currentCalculation.Text;
            string result;
            try
            {
                // Run it against the calculate function
                Calculator.Calculate(calculation, out result);
                // If it doesn't throw an exception, add to the history box
                AddCalcToHistory(result);
                // Set the current calculation box to the result
                currentCalculation.Text = result;
            }
            catch
            {
                // If it fails, show a popup window that the calculation failed.
                MessageBox.Show("Calculation failed, please check that the calculation was valid");
            }
        }

        private void Number_Click(object sender, EventArgs e)
        {
            Button button = sender as Button; // Set it as a button so we can reference the properties
            string input = button.Text; // Get the text on the button
            NumberProcess(input);
        }

        private void Operator_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            string input = button.Text;
            bool success = false;
            success = OperatorProcess(input); // Pass button text to operator process and get a true/false if it was successfull
            if (success) { LabelMod.Good(StatusLabel); } // If it was successful, make the label the success one
            else { LabelMod.Bad(StatusLabel); } // Otherwise show the input fail button.
        }

        private void Form_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool success = false;
            try
            {
                // Take the value of the key pressed as a string
                string input = e.KeyChar.ToString();
                try
                {
                    // Check if it is a number, if not, it will error and go to catch
                    success = Input.GetInt(input);
                    if (success)
                    {
                        // If it is a number between 0 and 9, add to the string
                        AppendCalc(input);
                        LabelMod.Good(StatusLabel);
                    }
                    else { LabelMod.Bad(StatusLabel); }
                }
                catch
                {
                    // Run the value against the operator processing
                    // If valid, good status label, otherwise bad
                    success = OperatorProcess(input);
                    if (success) { LabelMod.Good(StatusLabel); }
                    else { LabelMod.Bad(StatusLabel); }
                }
            }
            catch
            {
                LabelMod.Bad(StatusLabel);
            }
        }

        private void SaveFile_Click(object sender, EventArgs e)
        {
            try
            {
                string filePath;
                // Get the file path
                bool gotPath = SaveFileString(out filePath);
                // If function was successful
                if (gotPath)
                {
                    // Ensure that the file has the correct ending (.txt)
                    if (filePath.Length > 4)
                    {
                        string temp = filePath.Substring(filePath.Length - 4);
                        if (temp != ".txt")
                        {
                            filePath = filePath + ".txt";
                        }
                    }
                    else
                    {
                        filePath = filePath + ".txt";
                    }

                    using (StreamWriter sw = new StreamWriter(filePath))
                    {
                        // Take each line in the historybox and save to the file on a new line
                        foreach (string s in historyBox.Items)
                        {
                            sw.WriteLine(s);
                        }
                    }

                }
            }
            catch
            {
                // If the save fails, catch and show the message
                MessageBox.Show("Saving the file failed");
            }
        }

        private void OpenFile_Click(object sender, EventArgs e)
        {
            try
            {
                string filePath;
                // Get the file path to open
                bool gotPath = OpenFileString(out filePath);
                if (gotPath)
                {
                    // Ensure that the file has the correct ending (.txt)
                    if (filePath.Length > 4)
                    {
                        string temp = filePath.Substring(filePath.Length - 4);
                        if (temp != ".txt")
                        {
                            filePath = filePath + ".txt";
                        }
                    }
                    else
                    {
                        filePath = filePath + ".txt";
                    }

                    // Clear the current calculation and history
                    currentCalculation.Text = "";
                    historyBox.Items.Clear();
                    historyBox.Items.Add(" ");
                    using (StreamReader sw = new StreamReader(filePath))
                    {
                        string line;
                        // For each line in the file, remove the extras (answer and date) then process it like a normal calculation
                        while ((line = sw.ReadLine()) != null)
                        {
                            string temp = line;
                            if (temp.Contains(' '))
                            {
                                int index = temp.IndexOf(' ');
                                temp = temp.Remove(index);
                            }
                            currentCalculation.Text = temp;
                            Calculate_Click(sender, e);
                        }
                    }

                }
            }
            catch // Show an error message if it fails to open
            {
                MessageBox.Show("Processing the file failed");
            }
        }

        private bool NumberProcess(string input)
        {
            bool success = false;
            try
            {
                success = Input.GetInt(input); // Pass the text from the button to the function and confirm it is a valid choice

            }
            catch
            {
                LabelMod.Bad(StatusLabel); // Something went wrong, set the status message to the negative message
            }
            if (success)
            {
                AppendCalc(input); // Add the input to the end of the calculation
                LabelMod.Good(StatusLabel); // Set the status label to the positive message
            }
            else { LabelMod.Bad(StatusLabel); }

            return success;
        }

        private bool OperatorProcess(string input)
        {
            bool success = false;
            // Got through the permitted inputs and perform the corresponding action
            switch (input)
            {
                case "+":
                    success = AppendCalc("+");
                    break;
                case "-":
                    success = AppendCalc("-");
                    break;
                case "*(X)":
                case "*":
                case "X":
                case "x":
                    success = AppendCalc("*");
                    break;
                case "/(�)":
                case "/":
                    success = AppendCalc("/");
                    break;
                case ".":
                    success = AppendCalc(".");
                    break;
                case "C":
                case "c":
                    try { currentCalculation.Text = ""; success = true; }
                    catch { success = false; }
                    break;
                case "Clear":
                    try { historyBox.Items.Clear(); historyBox.Items.Add(" "); success = true; }
                    catch { success = false; }
                    break;
                case "SQRT()":
                    string temp = "SQRT(" + CreateSQRT() + ")";
                    if (temp != "SQRT()")
                    {
                        success = AppendCalc(temp);
                    }
                    break;
                case "^":
                    success = AppendCalc("^");
                    break;
                case "Backspace":
                case @"\b":
                    success = BackspaceCalc();
                    break;
                default:
                    success = false;
                    break;
            }
            return success;
        }

        private bool AddCalcToHistory(string result)
        {
            // Remove the placeholder space that stops the text box from showing the object name when empty
            if (historyBox.Items.Contains(" "))
            {
                historyBox.Items.Remove(" ");
            }
            if (currentCalculation.Text != null) // Confirm that the text has a value
            {
                try
                {
                    DateTime date = DateTime.Now;
                    string tempText = currentCalculation.Text + " = " + result + "  -  " + date.ToString("yyyy,MM,dd HH:mm:ss");
                    historyBox.Items.Add(tempText); // Add the calculation string to the textbox list
                    return true;
                }
                catch
                {
                    historyBox.Items.Add(" ");
                    return false;
                }
            }
            else
            {
                historyBox.Items.Add(" ");
                return false;
            }
        }

        private bool AppendCalc(string input)
        {
            string temp = currentCalculation.Text; // Take a backup of the string
            try
            {
                currentCalculation.Text = currentCalculation.Text + input; // Add the input to the end of the current calculation
                return true;
            }
            catch
            {
                currentCalculation.Text = temp; // Restore original in case of error
                return false;
            }

        }

        private string CreateSQRT()
        {
            // Create a popup that will generate the correct string format for a squareroot
            try
            {
                FormPopup inputForm = new FormPopup();
                return inputForm.ShowDialog() == DialogResult.OK ? inputForm.GetString() : "";
            }
            catch
            {
                LabelMod.Bad(StatusLabel);
                return "";
            }
        }

        private bool BackspaceCalc()
        {
            string temp = currentCalculation.Text;
            try
            {
                // Try to remove the last character from the string
                currentCalculation.Text = currentCalculation.Text.Remove(currentCalculation.Text.Length - 1);
                return true;
            }
            catch
            {
                // If it fails, restore the string from the backup
                currentCalculation.Text = temp;
                return false;
            }
        }

        private bool SaveFileString(out string path)
        {
            // Open the save file dialog box
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            // Limit to only .txt. files
            saveFileDialog.Filter = "txt files (*.txt)|*.txt";
            // Default to first (and only) item in the filter above
            saveFileDialog.FilterIndex = 1;
            // Save/Use the directory location for next time it opens
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                // If it submits (user doesn't exit) save the path
                path = saveFileDialog.FileName;
                return true;
            }
            else
            {
                path = "";
                return false;
            }
        }

        private bool OpenFileString(out string path)
        {
            // Open the dialog box
            OpenFileDialog openFileDialog = new OpenFileDialog();
            // Limit to .txt files
            openFileDialog.Filter = "txt files (*.txt)|*.txt";
            // Default to first item above
            openFileDialog.FilterIndex = 1;
            // Save/Use directory location whenever the dialog box is opened
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // If user submits, output the path
                path = openFileDialog.FileName;
                return true;
            }
            else
            {
                path = "";
                return false;
            }
        }
    }
}
