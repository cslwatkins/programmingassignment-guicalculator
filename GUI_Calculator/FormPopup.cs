﻿using GUI_Calculator.Tools;
using GUI_Calculator_Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI_Calculator
{
    public partial class FormPopup : Form
    {
        public FormPopup()
        {
            InitializeComponent();
        }

        private void ButtonSubmit_Click(object sender, EventArgs e)
        {
            if (InputString.Text.Length > 0)
            {
                // This is what the parent looks for to get the data and process the exit
                DialogResult = DialogResult.OK;
            }
            else
            {
                LabelMod.Bad(StatusLabel);
            }

        }

        private void InputButton_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            string input = button.Text;
            bool success = false;
            try
            {
                success = Input.GetInt(input); // Pass the text from the button to the function and confirm it is a valid choice

            }
            catch
            {
                if (input == "." && !InputString.Text.Contains('.'))
                {
                    success = true;
                }
            }
            if (success)
            {
                success = AppendCalc(input); // Add the input to the end of the calculation
                if (success)
                {
                    LabelMod.Good(StatusLabel); // Set the status label to the positive message
                }
                else { LabelMod.Bad(StatusLabel); }
            }
            else { LabelMod.Bad(StatusLabel); }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            // Close the window
            this.Close();
        }

        private bool AppendCalc(string input)
        {
            string temp = InputString.Text;
            try
            {
                // Add the input character to the end of the string
                InputString.Text = InputString.Text + input;
                return true;
            }
            catch
            {
                InputString.Text = temp;
                return false;
            }
        }

        public string GetString()
        {
            return InputString.Text;
        }

        private bool BackspaceCalc()
        {
            string temp = InputString.Text;
            try
            {
                // Removes the last character from the string
                InputString.Text = InputString.Text.Remove(InputString.Text.Length - 1);
                return true;
            }
            catch
            {
                InputString.Text = temp;
                return false;
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            bool success = BackspaceCalc();
            if (success)
            {
                LabelMod.Good(StatusLabel);
            }
            else { LabelMod.Bad(StatusLabel);}
        }
    }
}
