﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Calculator.Tools
{
    public static class LabelMod
    {
        public static bool Good(Label label)
        {
            try
            {
                label.Text = "Status: Good";
                label.ForeColor = Color.Green;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Bad(Label label)
        {
            try
            {
                label.Text = "Status: Input Failed";
                label.ForeColor = Color.Red;
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
