﻿namespace GUI_Calculator
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            button1 = new Button();
            button2 = new Button();
            button3 = new Button();
            button4 = new Button();
            button5 = new Button();
            button6 = new Button();
            button7 = new Button();
            button8 = new Button();
            button9 = new Button();
            Calculate = new Button();
            Add = new Button();
            Subtract = new Button();
            Multiply = new Button();
            Divide = new Button();
            button0 = new Button();
            currentCalculation = new TextBox();
            StatusLabel = new Label();
            historyBox = new ListBox();
            buttonDP = new Button();
            buttonClear = new Button();
            sqrt = new Button();
            Backspace = new Button();
            PowerOf = new Button();
            clearHistory = new Button();
            label1 = new Label();
            label2 = new Label();
            menuStrip1 = new MenuStrip();
            saveToolStripMenuItem = new ToolStripMenuItem();
            loadToolStripMenuItem = new ToolStripMenuItem();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new Point(81, 394);
            button1.Name = "button1";
            button1.Size = new Size(50, 50);
            button1.TabIndex = 0;
            button1.Text = "1";
            button1.UseVisualStyleBackColor = true;
            button1.Click += Number_Click;
            button1.KeyPress += Form_KeyPress;
            // 
            // button2
            // 
            button2.Location = new Point(137, 394);
            button2.Name = "button2";
            button2.Size = new Size(50, 50);
            button2.TabIndex = 1;
            button2.Text = "2";
            button2.UseVisualStyleBackColor = true;
            button2.Click += Number_Click;
            button2.KeyPress += Form_KeyPress;
            // 
            // button3
            // 
            button3.Location = new Point(193, 394);
            button3.Name = "button3";
            button3.Size = new Size(50, 50);
            button3.TabIndex = 2;
            button3.Text = "3";
            button3.UseVisualStyleBackColor = true;
            button3.Click += Number_Click;
            button3.KeyPress += Form_KeyPress;
            // 
            // button4
            // 
            button4.Location = new Point(81, 338);
            button4.Name = "button4";
            button4.Size = new Size(50, 50);
            button4.TabIndex = 3;
            button4.Text = "4";
            button4.UseVisualStyleBackColor = true;
            button4.Click += Number_Click;
            button4.KeyPress += Form_KeyPress;
            // 
            // button5
            // 
            button5.Location = new Point(137, 338);
            button5.Name = "button5";
            button5.Size = new Size(50, 50);
            button5.TabIndex = 4;
            button5.Text = "5";
            button5.UseVisualStyleBackColor = true;
            button5.Click += Number_Click;
            button5.KeyPress += Form_KeyPress;
            // 
            // button6
            // 
            button6.Location = new Point(193, 338);
            button6.Name = "button6";
            button6.Size = new Size(50, 50);
            button6.TabIndex = 5;
            button6.Text = "6";
            button6.UseVisualStyleBackColor = true;
            button6.Click += Number_Click;
            button6.KeyPress += Form_KeyPress;
            // 
            // button7
            // 
            button7.Location = new Point(81, 282);
            button7.Name = "button7";
            button7.Size = new Size(50, 50);
            button7.TabIndex = 6;
            button7.Text = "7";
            button7.UseVisualStyleBackColor = true;
            button7.Click += Number_Click;
            button7.KeyPress += Form_KeyPress;
            // 
            // button8
            // 
            button8.Location = new Point(137, 282);
            button8.Name = "button8";
            button8.Size = new Size(50, 50);
            button8.TabIndex = 7;
            button8.Text = "8";
            button8.UseVisualStyleBackColor = true;
            button8.Click += Number_Click;
            button8.KeyPress += Form_KeyPress;
            // 
            // button9
            // 
            button9.Location = new Point(193, 282);
            button9.Name = "button9";
            button9.Size = new Size(50, 50);
            button9.TabIndex = 8;
            button9.Text = "9";
            button9.UseVisualStyleBackColor = true;
            button9.Click += Number_Click;
            button9.KeyPress += Form_KeyPress;
            // 
            // Calculate
            // 
            Calculate.Location = new Point(192, 450);
            Calculate.Name = "Calculate";
            Calculate.Size = new Size(106, 50);
            Calculate.TabIndex = 9;
            Calculate.Text = "=";
            Calculate.UseVisualStyleBackColor = true;
            Calculate.Click += Calculate_Click;
            Calculate.KeyPress += Form_KeyPress;
            // 
            // Add
            // 
            Add.Font = new Font("Segoe UI", 12F);
            Add.Location = new Point(249, 338);
            Add.Name = "Add";
            Add.Size = new Size(50, 50);
            Add.TabIndex = 10;
            Add.Text = "+";
            Add.UseVisualStyleBackColor = true;
            Add.Click += Operator_Click;
            Add.KeyPress += Form_KeyPress;
            // 
            // Subtract
            // 
            Subtract.Font = new Font("Segoe UI", 12F);
            Subtract.Location = new Point(305, 338);
            Subtract.Name = "Subtract";
            Subtract.Size = new Size(50, 50);
            Subtract.TabIndex = 11;
            Subtract.Text = "-";
            Subtract.UseVisualStyleBackColor = true;
            Subtract.Click += Operator_Click;
            Subtract.KeyPress += Form_KeyPress;
            // 
            // Multiply
            // 
            Multiply.Location = new Point(248, 393);
            Multiply.Name = "Multiply";
            Multiply.Size = new Size(50, 50);
            Multiply.TabIndex = 12;
            Multiply.Text = "*(X)";
            Multiply.UseVisualStyleBackColor = true;
            Multiply.Click += Operator_Click;
            Multiply.KeyPress += Form_KeyPress;
            // 
            // Divide
            // 
            Divide.Font = new Font("Segoe UI", 12F);
            Divide.Location = new Point(305, 394);
            Divide.Name = "Divide";
            Divide.Size = new Size(50, 50);
            Divide.TabIndex = 13;
            Divide.Text = "/(÷)";
            Divide.UseVisualStyleBackColor = true;
            Divide.Click += Operator_Click;
            Divide.KeyPress += Form_KeyPress;
            // 
            // button0
            // 
            button0.Location = new Point(81, 450);
            button0.Name = "button0";
            button0.Size = new Size(50, 50);
            button0.TabIndex = 14;
            button0.Text = "0";
            button0.UseVisualStyleBackColor = true;
            button0.Click += Number_Click;
            button0.KeyPress += Form_KeyPress;
            // 
            // currentCalculation
            // 
            currentCalculation.Location = new Point(81, 162);
            currentCalculation.Margin = new Padding(2, 1, 2, 1);
            currentCalculation.Name = "currentCalculation";
            currentCalculation.Size = new Size(276, 23);
            currentCalculation.TabIndex = 15;
            currentCalculation.KeyPress += Form_KeyPress;
            // 
            // StatusLabel
            // 
            StatusLabel.AutoSize = true;
            StatusLabel.Font = new Font("Segoe UI", 11F);
            StatusLabel.ForeColor = Color.Green;
            StatusLabel.Location = new Point(254, 189);
            StatusLabel.Margin = new Padding(2, 0, 2, 0);
            StatusLabel.Name = "StatusLabel";
            StatusLabel.Size = new Size(93, 20);
            StatusLabel.TabIndex = 16;
            StatusLabel.Text = "Status: Good";
            // 
            // historyBox
            // 
            historyBox.FormattingEnabled = true;
            historyBox.HorizontalScrollbar = true;
            historyBox.ItemHeight = 15;
            historyBox.Items.AddRange(new object[] { " " });
            historyBox.Location = new Point(81, 73);
            historyBox.Margin = new Padding(2, 1, 2, 1);
            historyBox.Name = "historyBox";
            historyBox.Size = new Size(276, 79);
            historyBox.TabIndex = 17;
            // 
            // buttonDP
            // 
            buttonDP.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonDP.Location = new Point(137, 450);
            buttonDP.Name = "buttonDP";
            buttonDP.Size = new Size(50, 50);
            buttonDP.TabIndex = 18;
            buttonDP.Text = ".";
            buttonDP.UseVisualStyleBackColor = true;
            buttonDP.Click += Operator_Click;
            buttonDP.KeyPress += Form_KeyPress;
            // 
            // buttonClear
            // 
            buttonClear.Font = new Font("Segoe UI", 9F);
            buttonClear.Location = new Point(193, 226);
            buttonClear.Name = "buttonClear";
            buttonClear.Size = new Size(50, 50);
            buttonClear.TabIndex = 19;
            buttonClear.Text = "C";
            buttonClear.UseVisualStyleBackColor = true;
            buttonClear.Click += Operator_Click;
            buttonClear.KeyPress += Form_KeyPress;
            // 
            // sqrt
            // 
            sqrt.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point, 0);
            sqrt.Location = new Point(248, 282);
            sqrt.Name = "sqrt";
            sqrt.Size = new Size(50, 50);
            sqrt.TabIndex = 20;
            sqrt.Text = "SQRT()";
            sqrt.UseVisualStyleBackColor = true;
            sqrt.Click += Operator_Click;
            sqrt.KeyPress += Form_KeyPress;
            // 
            // Backspace
            // 
            Backspace.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point, 0);
            Backspace.Location = new Point(254, 226);
            Backspace.Name = "Backspace";
            Backspace.Size = new Size(102, 50);
            Backspace.TabIndex = 21;
            Backspace.Text = "Backspace";
            Backspace.UseVisualStyleBackColor = true;
            Backspace.Click += Operator_Click;
            // 
            // PowerOf
            // 
            PowerOf.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point, 0);
            PowerOf.Location = new Point(305, 282);
            PowerOf.Name = "PowerOf";
            PowerOf.Size = new Size(50, 50);
            PowerOf.TabIndex = 22;
            PowerOf.Text = "^";
            PowerOf.UseVisualStyleBackColor = true;
            PowerOf.Click += Operator_Click;
            // 
            // clearHistory
            // 
            clearHistory.Location = new Point(11, 73);
            clearHistory.Margin = new Padding(2, 1, 2, 1);
            clearHistory.Name = "clearHistory";
            clearHistory.Size = new Size(56, 77);
            clearHistory.TabIndex = 23;
            clearHistory.Text = "Clear";
            clearHistory.UseVisualStyleBackColor = true;
            clearHistory.Click += Operator_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label1.Location = new Point(26, 165);
            label1.Name = "label1";
            label1.Size = new Size(43, 20);
            label1.TabIndex = 24;
            label1.Text = "Input";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label2.Location = new Point(81, 48);
            label2.Name = "label2";
            label2.Size = new Size(134, 20);
            label2.TabIndex = 25;
            label2.Text = "Calculation History";
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { saveToolStripMenuItem, loadToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(381, 24);
            menuStrip1.TabIndex = 26;
            menuStrip1.Text = "menuStrip1";
            // 
            // saveToolStripMenuItem
            // 
            saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            saveToolStripMenuItem.Size = new Size(43, 20);
            saveToolStripMenuItem.Text = "Save";
            saveToolStripMenuItem.Click += SaveFile_Click;
            // 
            // loadToolStripMenuItem
            // 
            loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            loadToolStripMenuItem.Size = new Size(45, 20);
            loadToolStripMenuItem.Text = "Load";
            loadToolStripMenuItem.Click += OpenFile_Click;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(381, 520);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(clearHistory);
            Controls.Add(PowerOf);
            Controls.Add(Backspace);
            Controls.Add(sqrt);
            Controls.Add(buttonClear);
            Controls.Add(buttonDP);
            Controls.Add(historyBox);
            Controls.Add(StatusLabel);
            Controls.Add(currentCalculation);
            Controls.Add(button0);
            Controls.Add(Divide);
            Controls.Add(Multiply);
            Controls.Add(Subtract);
            Controls.Add(Add);
            Controls.Add(Calculate);
            Controls.Add(button9);
            Controls.Add(button8);
            Controls.Add(button7);
            Controls.Add(button6);
            Controls.Add(button5);
            Controls.Add(button4);
            Controls.Add(button3);
            Controls.Add(button2);
            Controls.Add(button1);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "MainForm";
            Text = "Calculator";
            KeyPress += Form_KeyPress;
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        private Button button5;
        private Button button6;
        private Button button7;
        private Button button8;
        private Button button9;
        private Button Calculate;
        private Button Add;
        private Button Subtract;
        private Button Multiply;
        private Button Divide;
        private Button button0;
        private TextBox currentCalculation;
        private Label StatusLabel;
        private ListBox historyBox;
        private Button buttonDP;
        private Button buttonClear;
        private Button sqrt;
        private Button Backspace;
        private Button PowerOf;
        private Button clearHistory;
        private Label label1;
        private Label label2;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem saveToolStripMenuItem;
        private ToolStripMenuItem loadToolStripMenuItem;
    }
}
